package components;

import print.ElaboratedTreePrinter;

import java.util.ArrayList;

/**
 * Créé par Gabriel le 22/11/2017.
 */
public class SymmetricalTree
{
    private Node rootNode;
    private SuitecaseItem[] valuesList;
    private ArrayList<SuitecaseItem> suitecaseItemsList;

    public SymmetricalTree(SuitecaseItem[] valuesList) {
        this.valuesList = valuesList;
    }

    public void elaboratedPrint() {
        ElaboratedTreePrinter.print(rootNode);
    }

    public void basicPrint() {
        rootNode.print();
    }

    public void create() {
        rootNode = new Node(new SuitecaseItem(0, 0));
        for (int i = 0; i < valuesList.length; i++) {
            addNode(new Node(valuesList[i]), rootNode);
        }
    }

    private void addNode(Node node, Node latestRoot) {
        if(latestRoot.getLeftChildren() == null && latestRoot.getRightChildren() == null) {
            latestRoot.setLeftChildren(new Node(node));
            latestRoot.setRightChildren(new Node(node));
        }
        else {
            addNode(node, latestRoot.getLeftChildren());
            addNode(node, latestRoot.getRightChildren());
        }
    }

    public ArrayList<SuitecaseItem> scanPossibilities() {
        suitecaseItemsList = new ArrayList<>();
        innerScan(rootNode, new SuitecaseItem(((SuitecaseItem)rootNode.getValue()).getWeight(), ((SuitecaseItem)rootNode.getValue()).getValue()));
        return suitecaseItemsList;
    }

    // Right children are counted for YES and left children for NO
    private void innerScan(Node node, SuitecaseItem item) {
        if(node.getLeftChildren() != null) {
            SuitecaseItem suitecaseItem = new SuitecaseItem(item.getWeight(), item.getValue());
            innerScan(node.getLeftChildren(), suitecaseItem);
        }
        if(node.getRightChildren() != null) {
            SuitecaseItem otherSuitecaseItem = new SuitecaseItem(item.getWeight() + ((SuitecaseItem)node.getLeftChildren().getValue()).getWeight(),
                    item.getValue() + ((SuitecaseItem)node.getLeftChildren().getValue()).getValue());
            innerScan(node.getRightChildren(), otherSuitecaseItem);
        }
        if(node.getLeftChildren() == null && node.getRightChildren() == null) {
            suitecaseItemsList.add(item);
        }
    }
}
