package components;

/**
 * Créé par Gabriel le 22/11/2017.
 */
public class SuitecaseItem
{
    private int weight;
    private int value;

    public SuitecaseItem(int weight, int value)
    {
        this.weight = weight;
        this.value = value;
    }

    public int getWeight()
    {
        return weight;
    }

    public int getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return String.format("W%s / V%s", weight, value);
    }
}
