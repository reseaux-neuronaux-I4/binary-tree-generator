package components;

import print.ElaboratedTreePrinter;

/**
 * Créé par Gabriel le 21/11/2017.
 */
public class Tree {
    private Node rootNode;
    private int[] valuesList;

    public Tree(int[] valuesList) {
        this.valuesList = valuesList;
    }

    public void elaboratedPrint() {
        ElaboratedTreePrinter.print(rootNode);
    }

    public void basicPrint() {
        rootNode.print();
    }

    public void create() {
        for (int i = 0; i < valuesList.length; i++) {
            if (rootNode == null) {
                rootNode = new Node(valuesList[i]);
            } else {
                addNode(new Node(valuesList[i]), rootNode);
            }
        }
    }

    private void addNode(Node node, Node latestRoot) {
        if (node != null) {
            if ((Integer)node.getValue() > (Integer)latestRoot.getValue()) {
                if (latestRoot.getRightChildren() == null) {
                    latestRoot.setRightChildren(node);
                } else {
                    addNode(node, latestRoot.getRightChildren());
                }
            } else {
                if (latestRoot.getLeftChildren() == null) {
                    latestRoot.setLeftChildren(node);
                } else {
                    addNode(node, latestRoot.getLeftChildren());
                }
            }
        }
    }
}
