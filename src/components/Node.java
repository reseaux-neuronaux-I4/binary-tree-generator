package components;

import print.PrintableNode;

/**
 * Créé par Gabriel le 21/11/2017.
 */
public class Node implements PrintableNode {
    private Object value;
    private Node leftChildren;
    private Node rightChildren;

    public Node(Object value) {
        this.value = value instanceof Node ? ((Node) value).value : value;
    }

    public Object getValue() {
        return value;
    }

    public Node getLeftChildren() {
        return leftChildren;
    }

    public void setLeftChildren(Node leftChildren) {
        this.leftChildren = leftChildren;
    }

    public Node getRightChildren() {
        return rightChildren;
    }

    public void setRightChildren(Node rightChildren) {
        this.rightChildren = rightChildren;
    }

    /* Methods for basic tree printing (own code) */
    public void print() {
        print("", true, "");
    }

    private void print(String prefix, boolean isTail, String precision) {
        System.out.println(prefix + (isTail ? "└── " : "├── ") + this.value + precision);
        if(leftChildren != null) {
            leftChildren.print(prefix + (isTail ? "    " : "│   "), false, " (L)");
        }
        if(rightChildren != null) {
            rightChildren.print(prefix + (isTail ? "    " : "│   "), false, " (R)");
        }
    }

    /* Implementing methods for advanced tree printing */
    @Override
    public PrintableNode getLeft() {
        return leftChildren;
    }

    @Override
    public PrintableNode getRight() {
        return rightChildren;
    }

    @Override
    public String getText() {
        return String.valueOf(value);
    }
}
