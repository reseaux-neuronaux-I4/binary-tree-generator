import components.SuitecaseItem;
import components.SymmetricalTree;
import components.Tree;

import java.util.Comparator;

/**
 * Créé par Gabriel le 21/11/2017.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("\n****** \n*** Creating and displaying a binary tree from a list of values *** \n******\n");
        int[] valuesList = {50, 20, 30, 40, 55, 60, 54};
        Tree tree = new Tree(valuesList);
        tree.create();
        tree.basicPrint();
        tree.elaboratedPrint();

        System.out.println("\n****** \n*** Creating a tree representing a suitcase completion *** \n******\n");
        SuitecaseItem[] suitecaseItemsList = {
            new SuitecaseItem(10, 5),
            new SuitecaseItem(5, 15),
            new SuitecaseItem(7, 3),
            new SuitecaseItem(5, 25),
            new SuitecaseItem(1, 50),
            new SuitecaseItem(4, 10),
            new SuitecaseItem(12, 50),
            new SuitecaseItem(8, 20)
        };
        SymmetricalTree symmetricalTree = new SymmetricalTree(suitecaseItemsList);
        symmetricalTree.create();
        symmetricalTree.basicPrint();
        symmetricalTree.elaboratedPrint();
        SuitecaseItem optimizedSuitecase = symmetricalTree.scanPossibilities().stream()
                .filter(o -> o.getWeight() <= 20)
                .max(Comparator.comparingInt(SuitecaseItem::getValue))
                .orElse(null);
        if(optimizedSuitecase == null) {
            System.err.println("Un problème est survenu lors du tri des valeurs optimales");
        }
        else {
            System.out.println(String.format("\nValeurs optimales de la valise : W%S / V%s", optimizedSuitecase.getWeight(), optimizedSuitecase.getValue()));
        }
    }
}
