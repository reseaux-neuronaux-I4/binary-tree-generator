package print;

/**
 * Créé par Gabriel on 22/11/17.
 */
public interface PrintableNode {

    /** Get left child */
    PrintableNode getLeft();

    /** Get right child */
    PrintableNode getRight();

    /** Get text to be printed */
    String getText();
}
